def find_in_file (fn, find="the"):
    try:
        with open (fn) as f:
            contents = f.read ()
    except FileNotFoundError:
        pass
    else:
        words = contents.split ()
        count = len (words)
        finded = contents.lower ().count(find)
        print (f"Finded {find} in file '{fn}' {finded} count")

find_in_file ("bio.txt")
find_in_file ("baron.txt")
find_in_file ("faust.txt")
