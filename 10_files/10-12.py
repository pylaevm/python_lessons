import json

filename = "user.json"

def get_fav_num (filename):
    fav_num = input ("Enter your favourite number: ")

    with open (filename, 'w') as fn:
        json.dump (fav_num, fn)
    return fav_num


def print_fav_num(filename):
    try:
        with open (filename) as fn:
            fav_num = json.load (fn)
    except FileNotFoundError:
        return None
    else:
        return fav_num

fn = print_fav_num(filename)

if fn:
    print (f"I knew you favorite number - {fn}")
else:
    fn = get_fav_num(filename)
    print (f"I'll remember you favourite number {fn} then you come back")

