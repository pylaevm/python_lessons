import json

filename = "user2.json"

def get_fav_num (filename):
    fav_num = input ("Enter your name: ")

    with open (filename, 'w') as fn:
        json.dump (fav_num, fn)
    return fav_num


def print_fav_num(filename):
    try:
        with open (filename) as fn:
            fav_num = json.load (fn)
    except FileNotFoundError:
        return None
    else:
        return fav_num


fn = print_fav_num (filename)

if fn:
    print (f"Hello, {fn}!")
    answer = input ("If it's not your name, type 'Yes': ")
    if answer == 'Yes':
        fn = get_fav_num (filename)
        print (f"I'll remember you  '{fn}' then you come back")

else:
    fn = get_fav_num (filename)
    print (f"I'll remember you  '{fn}' then you come back")

