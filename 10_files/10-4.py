fn = "guest.txt"

name = ""

with open(fn, 'w') as f:
    while name != 'quit':
        name = input ("Fill the guest name of party or 'quit' to finish entering: ")
        if name == 'quit':
            break
        string = f"The guest name is: {name}"
        f.write (string + "\n")
        print (string)