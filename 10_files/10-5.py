fn = "reasons.txt"

reason = ""

with open(fn, 'w') as f:
    while reason != 'quit':
        reason = input ("Give me the reason, why do you like programming: ")
        if reason == 'quit':
            break
        f.write (reason + "\n")