print ("Welcome to adding programm")
is_active = True
while is_active:
    try:
        a = int (input ("Enter first number: "))
        b = int(input ("Enter last number: "))
    except ValueError:
        print ("You entered not a number\nLet's try again")
        continue

    else:
        print (f"The result is {a + b}")
