def read_file (fn):
    try:
        with open(fn) as f:
            contents = f.read()
            print (contents)
    except FileNotFoundError:
        print (f"Can't find the file '{fn}'")

read_file("cats.txt")
read_file("dogs.txt")