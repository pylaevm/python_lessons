def read_file (fn):
    try:
        with open(fn) as f:
            contents = f.read()
            print (contents)
    except FileNotFoundError:
        pass

read_file("cats.txt")
read_file("dogs.txt")