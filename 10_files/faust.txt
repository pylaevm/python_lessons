Project Gutenberg's Faust: A Tragedy, by Johann Wolfgang von Goethe

This eBook is for the use of anyone anywhere in the United States and most
other parts of the world at no cost and with almost no restrictions
whatsoever.  You may copy it, give it away or re-use it under the terms of
the Project Gutenberg License included with this eBook or online at
www.gutenberg.org.  If you are not located in the United States, you'll have
to check the laws of the country where you are located before using this ebook.

Title: Faust: A Tragedy

Author: Johann Wolfgang von Goethe

Translator: John Stuart Blackie

Release Date: September 14, 2020 [EBook #63203]

Language: English

Character set encoding: UTF-8

*** START OF THIS PROJECT GUTENBERG EBOOK FAUST: A TRAGEDY ***




Produced by David Thomas





 FAUST: A TRAGEDY

 BY GOETHE

 TRANSLATED INTO ENGLISH VERSE
 WITH NOTES AND PRELIMINARY REMARKS

 By JOHN STUART BLACKIE
 PROFESSOR OF GREEK IN THE UNIVERSITY OF EDINBURGH

 SECOND EDITION
 CAREFULLY REVISED AND LARGELY REWRITTEN

 London
 MACMILLAN AND CO.
 1880


 TRANSLATOR'S DEDICATION.

 An Goethe.

 _Versuch ich's mich so kühnlich hoch zu heben,_
 _Zu den Gefilden reiner Lebensstrahlen?_
 _Und wag' ich's frech, mit schwacher Hand zu malen_
 _Was Dir nur ziemt, das buntbewegte Leben?_
 _Wie soll der Kinderzunge lallend Streben_
 _Aussprechen, was des Mannes Kraft gesungen?_
 _Wie soll des Menschen Stimme wiedergeben,_
 _Was aus der tiefen Götterbrust entsprungen?_
 _O! wenn der Liebe ungestümer Drang_
 _Mich trieb, dass ich das Heiligste entweihe,_
 _Und zu berauschter, frecher Sünde zwang;_
 _So schaue Du, aus der Verklärten Reihe,_
 _Aus Himmelsharfen liebevollem Klang,_
 _Und, wenn du mich nicht loben kannst, verzeihe!_


 CONTENTS.

 PREFACE.
 PRELIMINARY.
 DRAMATIS PERSONÆ.
 DEDICATION.
 PRELUDE AT THE THEATRE.
 PROLOGUE IN HEAVEN.
 FAUST.
 ACT I.
  Scene I, Scene II, Scene III.
 ACT II.
  Scene I, Scene II, Scene III, Scene IV, Scene V, Scene VI, Scene
  VII.
 ACT III.
  Scene I, Scene II, Scene III, Scene IV, Scene V, Scene VI, Scene
  VII, Scene VIII.
 ACT IV.
  Scene I, Scene II, Scene III, Scene IV, Scene V, Scene VI, Scene
  VII, Scene VIII, Scene IX.
 ACT V.
  Scene I, Scene II, Scene III, Scene IV, Scene V.
 FOOTNOTES.
 NOTES.


PREFACE.

/The/ appearance of this Second Edition of my translation of
"Faust," after an interval of more than forty years from the
publication of the original edition, may seem to require a word of
explanation. Very soon after the issue of the first edition I became
convinced that with the usual tendency of ambitious young men, I had
allowed my enthusiasm to overrule my discretion, and ventured upon a
task that demanded a much riper experience of life, and a much more
finished dexterity of execution than was to be expected from a
person of my age and capacity. I accordingly passed a verdict of
condemnation upon it, and--notwithstanding the more lenient sentence
passed on the work by not a few friendly voices--continued to regard
it as a juvenile performance, which had done the best service of
which it was capable, by teaching me my ignorance. This verdict was
confirmed in my mind by the appearance of the admirable version of
the same poem by my accomplished friend, Sir Theodore Martin, with
whose laurels, thus nobly earned, I was inclined to think it a sort
of impertinence to interfere. But, as time went on, and, while I was
employing my whole energies on laborious works in quite another
sphere, I still continued to hear people, whose judgment I could not
altogether despise, praising and quoting my "Faust;" in which
partial estimate they were no doubt confirmed by the approval of the
late George Lewes, in his classical Life of Goethe, and of the
Germans generally, who, from the close intercourse I have always
maintained with that people, are inclined to look on my doings in
the field of their literature with a specially favourable eye. Under
these circumstances, it was only natural for me to imagine that the
condemnation I had passed on my first juvenile attempt in verse had
perhaps been too severe; and that, after all, I owed it to myself,
and to Goethe, and to the noble people with whom I had been from my
youth so intimately connected, to give my translation a thorough
revisal, and to republish it in a form which might be as worthy of
the ambition that such an attempt implied as my literary capability
admitted. I accordingly, some four or five years ago, employed the
leisure of the summer months in correcting, and in not a few places
carefully rewriting, the whole work in the shape in which it now
appears.

The principal fault which led me to condemn so severely my early
work was a certain deficiency in the easy natural grace, which every
one who knows the great German poet must recognise as one of the
most attractive characteristics of his composition. This deficiency
arose in my case partly from want of experience in the dexterous use
of poetical expression, partly from the habit of clinging too
closely to the words of the original, which is the natural vice of a
young and conscientious translator. Long practice in such matters
has now convinced me that a literal version of a great poem never
can be a graceful version; and poetry without grace is like painting
without colour, or preaching without faith; it lacks the very
feature which makes it what it pretends to be, and gives it a right
to exist. Those who wish to be minutely curious about the _ipsissima
verba_ of a great poem should read a prose translation; the mere
want of the rhythmical movement never can deprive the work of its
ideal character and elevating influence; and in the case of Faust
this has been amply proved by the excellent translation of Mr.
Hayward, which, I believe, has now reached a twelfth edition. But
the problem of the poetical translator is to give, not the words,
but the character of the original; to transfer its spirit, its tone,
its salient features, and its rhythmical attitude, into another
tongue, so far as the capabilities of that other tongue render such
a transference possible. This is the principle on which I have
worked. It would have been easy for me to have made many passages
more literal; but, in doing so, I should have sacrificed the freedom
of handling, without which I am convinced that graceful ease and
naturalness in rhythmical composition is impossible.

There are some peculiarities in the rhythm of Faust to which it may
be as well specially to call the attention of the English reader.
While the fundamental metre is the octosyllabic Iambic, there is a
liberal use of the decasyllabic line, whenever the dignity of the
subject seems to require it, and not seldom, too, I fancy, from a
fine instinct which Goethe had to avert what Byron calls "the
fatal facility" of the octosyllabic stanza. This facility the
German poet counteracts also in another way, by the variety of the
places to which he attaches his rhyme; the couplet being constantly
varied with the quatrain, and that either in the way of the
alternate lines rhyming, or the first with the fourth, and the
second with the third. But a still more characteristic feature in
the rhythm of Faust is the frequent use of the Alexandrian line of
twelve syllables, and that, not as Pope and Dryden use it, for
giving greater volume and swell to a closing line, but simply to
indulge an easy motion, such as we may imagine a German to delight
in, when smoking his pipe and sipping his beer on a mild summer
evening, beneath the village lime tree. I request the English reader
particularly to note this peculiarity, and generally to tune his ear
to the varied flow of Goethe's easy rhythm; otherwise he will be
apt to blame the translator, who certainly is not bound to sacrifice
one of the most characteristic features of his author to propitiate
the favour of the most ignorant, the most uncultivated, and the most
lazy section of his readers. In the strictly lyrical parts of the
poem it will be found that, if not with curious minuteness,
certainly in general tone and effect, I have carefully followed the
movement of the original. To have done otherwise, indeed, would have
been difficult for me, to whom the movement of the original, in all
its changes, has long been as familiar as the responses of the
Church Service to a devout Episcopalian. Only let the reader not
expect from me any attempt to give back on every occasion the
trochaic rhymes or double endings, as we call them, of the original.
Such an attempt will only be made by the writer who is more anxious
to gain applause by performing a difficult feat, than to ensure
grace by conforming to the plain genius of the language in which he
writes.

                                                          J. S. B.

 /Altnacraig, Oban,/
 1_st October_ 1880.


 PRELIMINARY.

/The/ story of Dr. Faustus and the Devil is one of such deep human
significance, and, from the Reformation downwards, of such large
European reputation, that in giving some account of its origin,
character, treatment, legendary and poetical, I shall seem to be
only gratifying a very natural curiosity on the part of the
intelligent reader.

We, who live in the nineteenth century, in a period of the world's
intellectual development, which may be called the age of spiritual
doubt and scepticism, in contradistinction to the age of faith and
reverence in things traditional, which was first shaken to its
centre by the violent shock of the Reformation, can have little
sympathy with the opinions as to spiritual beings, demoniacal
agency, magic, and theosophy, that were so universally prevalent in
the sixteenth century. We believe in the existence of angels and
spirits, because the Scriptures make mention of such spiritual
beings; but this belief occupies a place as little prominent in our
theology, as its influence is almost null in regard to actual life.
In the sixteenth century, however, Demonology and Angelography were
sciences of no common importance; and were, too, a fruitful root
whence the occult lore of the sages, and the witch, ghost, and magic
craft of the many took their rise, and spread themselves out into a
tree, whose branches covered the whole earth with their shadow. From
the earliest Christian fathers, to the last lingering theosophists
of the seventeenth century, we can trace a regular and unshaken
system of belief in the existence of infinite demons and angels in
immediate connection with this lower world, with whom it was not
only possible, but of very frequent occurrence, for men to have
familiar intercourse. Psellus,[*i1] the "prince of philosophers,"
does not disdain to enter into a detailed account of the nature and
influence of demons, and seems to give full faith to the very
rankest old wives' fables of _dæmones incubi et succubi_,
afterwards so well known in the trials for witchcraft which
disgraced the history of criminal law not more than two centuries
ago. Giordano Bruno, the poet, the philosopher, and free-thinker
of his day, to whom the traditionary doctrines of the Church
were as chaff before the wind, was by no means free from the
belief in magic, the fixed idea of the age in which he lived. "O!
quanta virtus," says he, in all the ebullition of his vivid fancy,
"O quanta virtus est intersectionibus circulorum et quam sensibus
hominum occulta!!! cum caput draconis in sagittario exstiterit,
diacedio lapide posito in aqua, naturaliter (!) spiritus ad dandum
responsa veniunt."[*i2] The comprehensive mind of Cornelius
Agrippa, the companion of kings and of princes, soon sprung beyond
the Cabbalistical and Platonical traditions of his youth; but not
less is his famous book "De Philosophia Occulta" a good specimen
of the intellectual character of the age in which he lived. The
noted work "De Vanitate Scientiarum" is a child of Agrippa, not
of the sixteenth century. The names of Cardan, Campanella, Reuchlin,
Tritheim, Pomponatius, Dardi, Mirandula, and many others, might be
added as characteristic children of the same spirit-stirring era;
all more or less uniting a strange belief in the most baseless
superstitions, with deep profundity of thought, and comprehensive
grasp of erudition.

To understand fully the state of belief in which the intellect of
the sixteenth century stood in regard to magic, astrology,
theosophy, etc., it will be necessary to cast an eye back to the
early history of Christianity and philosophy.

There can, in the first place, be no doubt that the genius of the
Christian religion is completely adverse to that exaggerated and
superstitious belief in the power of the Devil and Evil Spirits,
which was so prevalent in the first ages of the Church, and
increased to such a fearful extent in the Middle Ages. The Jewish
religion, too, was founded on the great and fundamental doctrine
that there is but one God, as opposed to the Hindoo and Persian
notion of conflicting divinities, so universally spread over the
East; and all the wild waste of doctrines concerning demons
(διδασκαλίαι δαιμονίων, 1 Tim. iv. 1), with which the fertility of
Rabbinical invention overran the fair garden of Mosaic theology, has
been very properly relegated by German divines to its true source,
the Babylonish captivity. Such, however, is the proneness of human
reason to all sorts of superstition, that, though the New Testament
Scriptures expressly declare[*i3] that Jesus Christ came to annihilate
the power, and destroy the works of the Devil, the monotheism of
primitive Christianity was, in a few centuries, magnified into a
monstrous system of demonological theology, little better than
Oriental Dualism. The declension to this superstition was so much
the more easy, as there were not wanting certain passages of
Scripture (Eph. ii. 2, and vi. 12; 2 Thess. ii. 9), which ignorant
and bigoted priests could easily turn to their own purposes, in
magnifying this fancied power of the great enemy of man. A man like
Del Rio would find devils within the walls of the New Jerusalem; so
wonderfully sharp is his Jesuitical nose to scent out even the
slightest motion of infernal agency.

The Gnostic and Manichæan heresies which infested the Church during
the first five or six centuries could not be without their influence
in exalting the power of the principle of evil; but writers of a far
more philosophical character and more sober tone than those Oriental
heresiarchs cannot be exempted from the charge of having contributed
fairly to the same result. Of those fathers of the Church who did
not, like Arnobius and Lactantius, exclaim against all philosophy,
as opposed to the simplicity of the gospel, the greater number
belonged to the Alexandrian school of Neo-Platonists, who, with all
their sublime idealism, are known to have cherished, with a peculiar
fondness, some of the most childish and superstitious notions to
which philosophic mysticism has given birth. No lover of piety and
virtue springing from a high and soul-ennobling philosophy, but must
love and reverence the memory of such names as Proclus, Plotinus,
and Jamblichus. It cannot, however, be denied that the overstrained
ideas of these pure spirits went a great way to promote the growth
of the prevalent superstitions with regard to theurgy and magic. The
life of Plotinus seems, from the account given by Porphyry, to have
been considered by himself and his admirers as an uninterrupted
intercourse with spiritual intelligences, yea, with the one original
Spirit himself; and in the Enneads of this prince of philosophic
mystics, we have already fully developed all that system of mutual
sympathies and antipathies, of concords and discords, between the
all-animated parts of that mighty animal the World, which so readily
allowed themselves to be worked into a system of practical theurgy
and magic. Jamblichus, again, was not only a mystical philosopher,
who sought to arrive at union (ἕνωσις) with the Divine Being by
intellectual contemplation, but a magician and theurgist, as his
work on the Egyptian mysteries, and the many legends told of him by
his biographers, sufficiently prove.

I have been thus particular in holding forth the decidedly magical
and theurgic character of the Alexandrian School of Platonists, in
the second and third centuries, as it is easy to perceive that the
revival of the Platonic, or rather Neo-Platonic philosophy, on
occasion of the restoration of learning in the fourteenth and
fifteenth centuries, had a principal share in the formation of the
theosophic and magical views of the sixteenth century, which it is
my intention here to characterise. The world had become heartily
sick of the eternal boom-booming of the Aristotelian bitterns.[*i4]
The hungry spirit of man, aroused from its lethargic slumber,
demanded some more vital nourishment than the skeleton distinctions
of a thought-dissecting logic, and the vain pomposity of a learned
terminology, could afford; and when such men as Dante, Petrarch, and
Boccacio had taught the world to prefer the fulness of poetical life
to the nakedness of scholastic speculation, no wonder that Plato,
Plotinus, and Proclus, when brought into the West by the learned
fugitives of Constantinople, should have received a hearty welcome,
and exercised a deep-spread influence over the philosophy of the
succeeding centuries. Gemistus Pletho, Bessarion, and Marsilius
Ficinus, are well known as the three principal restorers of the
Platonic philosophy in the fifteenth century: but it deserves
especially to be remarked, that these men were far from being pure
worshippers of their great master, but mixed it up with the theurgic
dreamings of Jamblichus and Porphyry, nay, even went as far back as
Pythagoras and Hermes Trismegistus, and held the simple Platonic
doctrines as of comparatively little consequence, unless taken in
connection with the mighty system which, out of such strange
materials, had been built up by the Neo-Platonists.[*i5]

In connection with the revival of the Platonic philosophy in Italy,
we cannot omit to mention the name of Reuchlin, whose zeal for
cabbalistical studies is said to have been first excited by the
famous Johannes Picus Mirandula.[*i6] Reuchlin was a German, and is
the more interesting to us as the contemporary, or rather the master
and instructor of Agrippa, Melancthon, and many celebrated men of
the sixteenth century, whose names stand immediately connected with
the story of Doctor Faust. To complete the wild dreamings of the
Italian Platonists, nothing was now wanting but a revival of the
Rabbinical and Talmudistic lore; and Reuchlin, whom Europe still
reveres as the father of Hebrew learning in modern Theology, was
precisely the man for this purpose. It was natural that the language
of the sacred Book should have been considered as containing
something mystical and transcendental even in its very letters; and
we need not wonder that the enthusiasm of the first Hebrew scholars
in Germany should have discovers the key of all the sciences in that
cabbalistic lore, which we are now accustomed to use in common
discourse, as a synonym for the most childish and unintelligible
jargon.

Taking, thus, the prevailing theology of the Church, in connection
with the impulse which the human mind had received from the revival
of the Platonic philosophy, and the strong reaction, which the
risings of independent thought in the breasts of men like Telesius,
Campanella, and Bruno, had raised against the long-established
despotism of the Aristotelian philosophy,--and all this worked up to
a point by the revival of Cabbalism, through Reuchlin and other
cultivators of Oriental literature,--we shall have no difficulty in
perceiving at once the leading features of the age in which Faust
flourished, and the causes which led to their development. We see
the human intellect, in being roused into new life from the icy
night of scholasticism, surrounded by the glowing but unsubstantial
morning-clouds of a philosophy of feeling and imagination.
Sufficiently occupied with gazing, child-like, on the hovering
shapes that teemed so richly from its new-awakened being, it had no
tune, no wish, to enter upon the severe task of conscious manhood,
that of criticising its own powers, and defining, with cautious
precision, what the mind of man can know, and what it cannot
know,--and was thus destined, for a short season, to flounder
through the misty regions of theosophy and magic, till it should
learn, from experience, to find at once its starting-point and its
goal, in the exhaustless fulness of actual Nature.

In such an age, and under the influence of opinions, religious and
philosophical, so different from those now prevalent, flourished the
mysterious hero of modern magic, whom the pen of Goethe has made,
likewise, one of the principal heroes of modern poetry. That a good
deal of obscurity should have gathered around such a
character,--that the love of the marvellous should have united with
the ignorance of the age, in magnifying juggling tricks into
miracles of magic, and clouding with a poetical mistiness that which
was clear and definite,--is not to be wondered at. But that such a
character actually existed, the tradition perpetuated from age to
age on its native soil, and found, with little variation, scattered
over almost every country, and clothed in almost every language of
Europe, is of itself sufficient evidence. Popular legends seldom
spring, like the antediluvian and prelapsarian traditions of the
Talmudists, or the genealogies of old Celtic families, from mere
airy nothingness; and, however contradictory and inconsistent their
integrant parts may appear, they have all formed themselves around a
nucleus of substantial reality. Nevertheless, as there is nothing so
absurd which has not been asserted by some one of the philosophers,
so there have not been wanting men of learning and investigation,
who have seriously set themselves to the task of proving away the
personality of the renowned Doctor Faust.[*i7] But to detect a few
chronological inaccuracies in the common popular legend, and to hold
out to merited contempt the silliness, and even the impossibility of
many things contained in it, may afford an opportunity for the
display of a pedantic erudition, but can give no ground for the
sweeping conclusion that the person, of whom these stories are told,
did actually never exist. The monks were clever fellows; but, with
all their ability, they would have found it difficult to invent such
a story as Faust--so generally believed--out of mere nothing. The
sceptics themselves are sensible of this; and, accordingly, Dürr,
the chief of them, while he denies the personality of Faust the
magician, endeavours to give a probable reason for the prevalence of
the story, by throwing the whole burden upon the back of Faust the
printer, father-in-law of Peter Schoeffer, and fellow-workers both
of Guttenburg,--the famous trio, among whom the honour of the
invention of printing is divided. The envy of the monks, acting on
the ignorance of the age, here comes most opportunely into play, to
explain how the inventor of such a novel art of multiplying books
should have been generally accounted a magician. There can, indeed,
be little doubt that he was so accounted by many ignorant people;
and as this idea is sufficiently poetical, Klingemann has taken
advantage of it in his tragedy of Doctor Faust.[*i8] The main
objection, however, on the face of this theory, is, that all the
legends of Faust agree in placing the hero of magic fully half a
century later than Faust the printer, who flourished about 1440. It
is true, indeed, that some of the _Volksbücher_ (_vide_ Dürr, _ut
supra_) ascribe to the Emperor Maximilian, what is generally told of
Charles V., viz. that Doctor Faust conjured up before him the
apparitions of Alexander the Great and his queen; but the other
tricks, which were played before Cardinal Campegio and Pope Adrian,
agree better with the age of Charles V. than with that of
Maximilian. It is quite possible, however, that Faust may have
exhibited his magical skill before both these emperors, whose reigns
occupied the space from 1492 to 1558, Maximilian dying in 1519; for
even the date of Maximilian will never bring us back to the era when
Faust the printer was in his glory.

The personality of Faust, however, is not left to rest upon the mere
traditionary evidence of the vulgar legend. The diligence of German
antiquaries, even before Goethe's Faust gave importance to the
theme, had collected many trustworthy historical testimonies in
confirmation of the common belief. Dürr's Letter on this subject
is dated 1676; and, not seven years afterwards, appeared Neumann's
historical disquisition _De Fausto praestigiatore_. This essay I
have not seen at full length; but from the epitome given of it by
Hauber (_Bibliotheca Magica_, vol. ii. p. 706), I fear that there
may be but too much cause for the remark of Heumann,[*i9] that "it
smacks too much of the young graduate." It was certainly a very
pious motive that induced Neumann, a student of Wittenberg, to
attempt removing from his _alma mater_ the shame of having given
birth, or even education, to such a notorious character as Doctor
Faust; but truth often forces us to admit what fondest prejudice
would fain deny. The next critical essay on Faust, is that of
Heumann, just quoted, in Hauber's Library of Magic, and it
contains the most important of these historical testimonies to the
truth of the Faustish legend, which have since been so
comprehensively exhibited in one work by Doctor Stieglitz.[*i10]

As all the traditions agree in representing Faust as haying studied
at Wittenberg, and there, too, exhibited a number of magical tricks
to his good friends the students, it was natural to suspect that
Luther or Melancthon should, somewhere or other, make mention of
such a notorious character. And, accordingly, Stieglitz follows
Horst (_Zauber-Bibliotheck_, vi. 87) in asserting that Melancthon
actually does make mention of Doctor Faust in one of his epistles;
but as neither of these writers cites the passage, or mentions in
what particular part of Melancthon's work it is to be found, I
barely mention this circumstance on their authority. There is,
however, very great probability that the testimony of Joannes
Manlius, in his Collectanea, the principal one relied on both by
Heumann and Stieglitz, is, in reality, to be considered as a
testimony of Melancthon. Manlius himself[*i11] says of his
Collectanea, "_Labor hic noster collectus ex ore D. Phillippi
Melanchthonis allisque clarissimis viris_," and might, on this
account, as Heumann remarks, have fitly been named _Melancthoniana_,
or Melancthon's Table-Talk. But be this as it may, Manlius'
testimony is most decided, and runs as follows:--"I was acquainted
with a certain person, called Faust of Kundling, a small town in
Wurtemberg. He was a Cracovian Scholasticus, and read lectures on
magic in the university there. He was a great rambler (_vagabatur
passim_), and possessed many secrets. At Venice, wishing to amuse
the populace, he boasted that he would fly up to heaven. The devil
accordingly wafted him up a certain height, but dashed him down
again in such a plight, that he lay half-dead on the ground. A few
years ago, the same John Faust, on the last day of his life, was
found sitting in the common inn of a certain village in the Duchy of
Wittenberg. He was, indeed, a most vile blackguard (_turpissimus
nebula_), of a most filthy life, so much so, indeed, that he once
and again almost lost his life on account of his excesses. The
landlord of the inn asked him why he sat there so sad, contrary to
his wont? "Be not terrified if you shall hear anything on this
night," was his short answer. And at midnight the house was
shaken. Next morning, near mid-day, as Faust did not make his
appearance, the landlord entered into his chamber, and found him
lying beside his bed, with his face on the ground, having been so
slain by the devil. When he was yet alive, he was accompanied by a
dog, which was the devil. ... This Faust the magician, a most vile
beast, and a common sewer of many devils (_cloaca multorum
diabolorum_), was also a great boaster, and pretended that all the
victories of the Imperial armies in Italy were gained by the help of
his magic."[*i12] With this account agrees exactly that given by
Wier,[*i13] the disciple and confidant of the celebrated Cornelius
Agrippa von Nettesheim. Del Rio,[*i14] who wrote at the end of the
sixteenth century, introduces him along with the same Agrippa,
playing tricks on the poor landlords, with whom they sojourned in
their vagabond excursions, by paying them with money which turned
into crumbs and chaff, whenever the magicians were out of sight; but
his connection with such a philosopher as Agrippa is much to be
doubted, as Wier has not even hinted at it in the passage where he
treats expressly of the Doctor.

The only other contemporary writer from whom I shall quote at
length, is Begardi[*i15] whose book, _Zeyger der Gesundheit_," was
published in 1539, and contains the following interesting testimony
to the age and character of Faust, which I give here from the
German, as it stands in Dr. Stieglitz's essay.

"There is yet a celebrated character whom I would rather not have
named; but since I must mention him, I will tell what I know of him
in a few words. Some years ago this man passed through almost all
lands, princedoms, and kingdoms, making his name known to everybody,
and making great show of his skill, not in medicine only, but in
chiromancy, necromancy, physiognomy, visions in crystals, and such
like. And in these things he not only acquired great notoriety, but
also obtained the name of a famous and experienced master. He did
not conceal his name, but called himself Faust, and used to
subscribe himself _philosophus philosophorum_. But of those who were
cheated by him, and complained of the same to me, there is a great
multitude. His promise was great like that of Thessalus in Galen's
days, as also his fame like that of Theophrastus;[*i16] but his
deeds, as I have heard, were almost always found to be very petty
and deceitful, though he was, to speak plainly, not slow at giving,
and especially taking, money, as many a worthy person had cause to
know. But now the matter is not to be remedied; past is past, and
gone is gone. I must even leave the matter as it is; and see thou to
it, that thou treat it as a good Christian ought to do."

Thus far Begardi in his honest naïve language. Heumann cites
further a long passage from Tritheim's Epistolæ Familiares,[*i17]
describing a character altogether similar to that above described by
Manlius and Begardi; with this remarkable difference, that he is not
called Doctor John Faust, as he is by Manlius, and in all the vulgar
traditions, but "_Magister Georgius Faustus Sabellicus, Faustus
Junior._" I think Stieglitz has been too precipitate in concluding
that difference in the name must necessarily imply a difference in
the person. The vagabond wonder-workers of those days were wont to
have a number of names, as the example of Paracelsus alone is
sufficient to show. With regard to the denomination of "Faustus
_junior_," this cannot certainly refer to our John Faust, with
whom this George (if he was a different person) must have been
contemporary. It probably relates to Faust the printer, who has also
been accused of magic, or to some other Faust of the fifteenth
century, whose fame has been now swallowed up in that of Doctor John
Faust of Wittenberg.

Camerarius and Gesner[*i18] also make mention of Doctor Faust; but
let the passages already quoted suffice to prove the historical
reality of our magical hero.

Joining together these historical testimonies and the popular
traditions, it is not difficult to come to a pretty accurate
conclusion as to the real character of Doctor Faust. He appears to
have been a man of extensive learning, especially in medical and
astrological, perhaps too in philological and theological, science.
But, driven by a restless spirit, and a vain desire of popular
applause, he seems to have early abandoned the calm and steady path
that leads to professional eminence, and sought after that noisy but
less substantial fame, which his scientific skill was fitted to
procure for him in the eyes of the gazing multitude. Many of the
greatest philosophers, indeed, as Solomon, Roger Bacon, and
Cornelius Agrippa, have been accounted magicians for no other reason
than their uncommon wisdom, far surpassing that of the age in which
they lived; but there is too much reason to suspect that Faust's
fame as a magician rests upon much more questionable grounds, and
the whole account of his life and exploits leaves upon our mind the
impression that he was a very clever vagabond quack, rather than a
retired and contemplative philosopher. There is much in all that is
told of him that recalls to our mind the biography of Paracelsus, a
man certainly of great genius, but of much greater impudence, who
gained his living by acting upon the folly of mankind.[*i19] By all
accounts, indeed, Faust was a man of much more distinguished
academic learning than Paracelsus, of whom historians even question
whether he ever studied at any university; but as a vagabond, a
boaster, and a wonder-promiser, the one is perhaps only not superior
to the other. With a little knowledge of medicine, a little
classical lore, some dexterity in performing sleight-of-hand
wonders, and a panoply of assurance, a clever man like Faust or
Paracelsus may easily obtain a livelihood, and, what is more, an
imperishable name. For such characters a strolling life is at once a
pleasure and a necessity. Paracelsus soon lost his chair at
Basle,--for a man is never a hero to his _valet-de-chambre_,--and,
if we may believe the common legend, Faust scarcely left a corner of
the earth unvisited, and filled Asia and Europe with his renown.

And verily he has had his reward. Since the time of his death, not
only Germany, but England, France, and Holland, have swarmed with
"prodigious and lamentable histories" of the "great magician
John Faust, with his testament and his terrible death." Magical
books under his name have become as famous as those of
Solomon;[*i20] artists and poets have vied with one another in
rendering his name immortal in the annals of Art; tragedies and
comedies, puppet-plays and operas, ballads and novels, essays, and
dissertations and commentaries, prologues and epilogues, and all the
varied paraphernalia of genius and erudition, have been heaped on
one another, to adorn the trophy of Doctor John Faustus, the great
German quack. The wondrous exploits of Faust are endless, and it
would be an endless task to recount the tithe of them. Were I to
enter upon an exposition of how Doctor Faust first cited
Mephistopheles on a crossroad in the midst of a dark fearful wood
near Wittenberg,--how the Devil visited him frequently in his own
study in all shapes and sizes,--how the Doctor was, after some
hesitation, prevailed on to sell his soul to Lucifer, and to that
effect signed a formal bond with blood drawn from his own arm,--how
he neglected all the warnings of his good genius, and even the
terrible writing that appeared on his wounded arm, /Homo Fuge/!--how
the wily Devil dissuaded him from the quiet of a domestic life, when
he wished to marry, that he might drag him into all kinds of
licentiousness,--how he forced Mephistopheles to answer all his
importunate interrogatories, as to the state of Hell, and the
condition of the damned, which the Devil painted in colours as
terrible as if he had been an Evangelist of the north-west Highland
type,--how Faust was transported into Hell upon the back of
Beelzebub, and left floundering through the chaos of the abyss,--how
he travelled from star to star, and surveyed all the infinity of
worlds, with as much expedition as the imagination of a modern
poet,--how he turned astrologer, and vied with the fame of
Nostradamus,--how he wandered over the whole world, and saw Rome,
which is a city where there is a river called Tiber, and Naples,
which is the birthplace of Virgil, who was also a great magician,
and caused a passage to be made through the rock of Posilippo, in
one night, a whole mile long,--how he played the devil in the
Sultan's seraglio, and passed himself off for Mahomet with the
ladies of the palace,--how he sat invisible at the Pope's banquet,
and whipped away all the tit-bits from the plates of Pope Adrian and
his assessors of the scarlet stockings, so that his Holiness was
obliged to believe that some tormented soul from Purgatory was
haunting the Vatican, and ordered prayers to be made
accordingly,--how he further showed his enmity to the Church by
making secret broaches in the wine-casks of the Bishop of
Saltzburg's cellar, and being on one occasion surprised by the
butler, perched the poor wretch upon a tree, where he sprawled like
a limed bird for the whole length of a frosty night,--how he called
up the apparition of Alexander the Great and his Queen before the
Emperor Charles V., who assured himself of the reality of this
vision by touching the wart which history reports to have been upon
the hero's neck,--how in like manner he frightened the students of
Erfurt by raising the ghost of Polypheme, and bewitched his good
friends the students, and himself to boot, by the apparition of the
beautiful Helena,--how he bamboozled a boor by promising him a penny
for as much hay as he could eat from his waggon, and then swallowing
the whole cart-load down, as easily as it had been a spoonful of
Sauerkraut,--how he sold a fine horse for a small price to a jockey,
who, delighted with the bargain, set off galloping upon this
wightest of steeds, till he came to a running stream, in the middle
of which, and just where the water was deepest, the animal all at
once changed into a bottle of straw, and left the 
