class Employee(name, soname, price):
    def __init__(self, name, soname, price):
        self.name = name
        self.soname = soname
        self.price = price

    def give_raise (self, addon=5000):
        if addon < 0:
            return
        self.price += addon
