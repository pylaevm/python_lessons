def print_city (city, country, population = ''):
    if population:
        return (f"{city.title()},{country.upper()} population = {population}")
    else:
        return (f"{city.title()},{country.upper()}")