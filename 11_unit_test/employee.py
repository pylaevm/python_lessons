class Employee():
    def __init__(self, name, soname, price):
        self.name = name
        self.soname = soname
        self.price = int(price)

    def give_raise (self, addon=5000):
        if addon < 0:
            return None
        self.price += int(addon)
