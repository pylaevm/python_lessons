import unittest
from city_functions import print_city

class NamesTestCase(unittest.TestCase):
    def test_print_city (self):
        mes = print_city ("santiago", "usa")
        self.assertEqual (mes, 'Santiago,USA')

    def test_print_city_with_population (self):
        mes = print_city ("santiago", "usa", "5000000")
        self.assertEqual (mes, 'Santiago,USA population = 5000000')


if __name__ == '__main__':
    unittest.main()


