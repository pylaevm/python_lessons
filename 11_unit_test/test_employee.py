from employee import Employee
import unittest

class TestEmployee(unittest.TestCase):
    """docstring for TestEmployee"""
    def setUp(self):
        self.employee = Employee('Maxim', 'Pylaev', '60000')

    def test_default_addon(self):
        price = int(self.employee.price) + 5000
        self.employee.give_raise ()
        self.assertEqual (self.employee.price, price)

    def test_twenty_addon(self):
        price = int(self.employee.price) + 20000
        self.employee.give_raise (20000)
        self.assertEqual (self.employee.price, price)

if __name__ == '__main__':
    unittest.main()