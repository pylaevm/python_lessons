import sys

import pygame


class Persona(object):
    """класс для управления ресурсами и поведением игры Persona"""
    def __init__(self):
        pygame.init ()
        self.screen = pygame.display.set_mode ((0, 0), pygame.FULLSCREEN)
        pygame.display.set_caption ("Space")
        self.screen_rect = self.screen.get_rect()

        bg = pygame.image.load('images/space.jpg')
        self.bg_rect = self.screen.get_rect()
        self.bg_image = pygame.transform.scale(bg, (self.bg_rect.width, self.bg_rect.height))

        image = pygame.image.load('images/ship.png').convert_alpha()
        self.image = pygame.transform.scale(image, (50,100))

        # Позиционирование на экране
        self.rect = self.image.get_rect()
        print (self.rect)
        # Новый корабль появляется в нижнем центре окна
        self.rect.center = self.screen_rect.center

        self.move_right = False
        self.move_left = False
        self.move_top = False
        self.move_bottom = False

        self.ship_speed = 3.2
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

    def run_game(self):
        """Запуск основного цикла игры"""
        #self.bg_track.play(-1, 0, 500)
        while True:
            self._check_events()
            self._update_ship()
            self._update_screen()

    def _check_events(self):
        # Отслеживание событий клавиатуры и мыши
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)


    def _check_keydown_events (self,event):
        if event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_RIGHT:
            self.move_right = True
        elif event.key == pygame.K_LEFT:
            self.move_left = True
        elif event.key == pygame.K_UP:
            self.move_top = True
        elif event.key == pygame.K_DOWN:
            self.move_bottom = True


    def _check_keyup_events (self,event):
        if event.key == pygame.K_RIGHT:
            self.move_right = False
        elif event.key == pygame.K_LEFT:
            self.move_left = False
        elif event.key == pygame.K_UP:
            self.move_top = False
        elif event.key == pygame.K_DOWN:
            self.move_bottom = False


    def _update_ship (self):
        if self.move_right and self.rect.right < self.screen_rect.right:
            self.x += self.ship_speed
        if self.move_left and self.rect.left > 0:
            self.x -= self.ship_speed
        if self.move_top and self.rect.top > 0:
            self.y -= self.ship_speed
        if self.move_bottom and self.rect.bottom < self.screen_rect.bottom:
            self.y += self.ship_speed

        self.rect.x = self.x
        self.rect.y = self.y


    def _update_screen(self):
        self.screen.blit(self.bg_image, self.bg_rect)
        self.screen.blit(self.image, self.rect)
        # Отображение последнего прорисованного экрана
        pygame.display.flip()



if __name__ == '__main__':
    ai = Persona()
    ai.run_game()