import sys
import pygame
from random import randint


from star_my import Star

class Raindrop(object):
    """класс для управления ресурсами и поведением игры Raindrop"""
    def __init__(self):
        pygame.init ()
        self.screen = pygame.display.set_mode ((0, 0),pygame.FULLSCREEN)
        pygame.display.set_caption ("Star ceils")
        self.rect = self.screen.get_rect()

        self.bg_color = (30, 70, 150)
        #Пришельцы
        self.stars = pygame.sprite.Group()

        #Создание флота пришельцев
        self._create_fleet()

    def run_game(self):
        """Запуск основного цикла игры"""
        while True:
            self._check_events()
            self._update_screen()

    # Отслеживание событий клавиатуры и мыши
    def _check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)

    def _check_keydown_events (self,event):
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = True
        elif event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()


    def _check_keyup_events (self,event):
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = False

    #Создание флота 
    def _create_fleet(self):
        star = Star(self)
        star_width, star_height = star.rect.size
        star_number_x = self._get_star_number(star_width)
        row_numbers = self._get_row_number(star_height)

        for row_number in range(row_numbers):
            for star_number in range(star_number_x):
                self._create_star(star_number, row_number)

    # Количество в ряду
    def _get_star_number(self, star_width):
        available_space_x = self.rect.width - (2 * star_width)
        number_stars_x = available_space_x // (2 * star_width)
        return number_stars_x

    #Количество пришельцев
    def _get_row_number(self, star_height):
        #Определяю количество рядов на экране
        available_space_y = self.rect.height - 2 * star_height

        number_rows = available_space_y // (2 * star_height)
        return number_rows

    #Создание одного пришельца
    def _create_star(self, star_number, row_number): 
        star = Star(self)
        star_width, star_height = star.rect.size
        star.x = star_width + 2 * star_width * star_number
        rand_x = randint (-10, 10)
        star.x += rand_x
        star.rect.x = star.x
        star.y = star_height + 2 * star_height * row_number
        rand_y = randint (-10, 10)
        star.y += rand_y
        star.rect.y = star.y
        self.stars.add(star)

    def _update_screen(self):
        self.screen.fill(self.bg_color)

        self.stars.draw(self.screen)

        # Отображение последнего прорисованного экрана
        pygame.display.flip()



if __name__ == '__main__':
    ai = Raindrop()
    ai.run_game()