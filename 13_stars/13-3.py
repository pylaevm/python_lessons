import sys
import pygame
from random import randint

from drop import Drop

class Raindrop(object):
    """класс для управления ресурсами и поведением игры Raindrop"""
    def __init__(self):
        pygame.init ()
        self.screen = pygame.display.set_mode ((0, 0),pygame.FULLSCREEN)
        pygame.display.set_caption ("Raindrop")
        self.rect = self.screen.get_rect()

        self.bg_color = (30, 100, 90)
        
        self.drops = pygame.sprite.Group()
        self.dropn = 0
        self.pause_rain = False

        #Создание дождя
        self._create_raindrop()

    def run_game(self):
        """Запуск основного цикла игры"""
        while True:
            self._check_events()
            if self.pause_rain == False:
                self._update_drop()
            self._update_screen()

    # Отслеживание событий клавиатуры и мыши
    def _check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)

    def _check_keydown_events (self,event):
        if event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self.pause_rain = not self.pause_rain


    #Создание флота 
    def _create_raindrop(self):
        drop = Drop(self)
        drop_width, drop_height = drop.rect.size
        drop_number_x = self._get_drop_number(drop_width)
        #print (drop_number_x)
        row_numbers = self._get_row_number(drop_height)
        #print (row_numbers)

        for row_number in range(row_numbers):
            for drop_number in range(drop_number_x):
                self._create_drop(drop_number, row_number)

    # Количество капель в ряду
    def _get_drop_number(self, drop_width):
        available_space_x = self.rect.width - (2 * drop_width)
        number_drops_x = available_space_x // (2 * drop_width)
        return number_drops_x

    #Количество строк
    def _get_row_number(self, drop_height):
        #Определяю количество рядов на экране
        available_space_y = self.rect.height - 2 * drop_height

        number_rows = available_space_y // (2 * drop_height)
        return number_rows

    #Создание одной капли
    def _create_drop(self, drop_number, row_number): 
        drop = Drop(self)
        drop_width, drop_height = drop.rect.size
        drop.x = drop_width + 2 * drop_width * drop_number
        rand_x = randint (-5, 5)
        drop.x += rand_x
        drop.rect.x = drop.x
        drop.y = drop_height + 2 * drop_height * row_number
        rand_y = randint (-5, 5)
        drop.y += rand_y
        drop.rect.y = drop.y
        #print (f"drop [{row_number},{drop_number}] coords {drop.rect}")
        self.drops.add(drop)

    def _update_drop(self):
        #Обновление позиций снарядов
        self.drops.update()

        #Удаление старых снарядов
        for drop in self.drops.copy():
            if drop.rect.bottom >= self.rect.bottom:
                self.drops.remove(drop)
                self._create_drop(self.dropn, 0)
                self.dropn+=1
                self.dropn %= self._get_drop_number(drop.rect.width)

    def _update_screen(self):
        self.screen.fill(self.bg_color)

        self.drops.draw(self.screen)

        # Отображение последнего прорисованного экрана
        pygame.display.flip()



if __name__ == '__main__':
    ai = Raindrop()
    ai.run_game()