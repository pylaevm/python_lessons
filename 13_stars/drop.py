import pygame
from pygame.sprite import Sprite

class Drop(Sprite):
    """docstring for Drop"""
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen

        image = pygame.image.load ('image/drop.png').convert_alpha()
        self.image = pygame.transform.scale(image, (50, 50))
        self.rect = self.image.get_rect()

        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        self.y = float (self.rect.y)
        self.x = float (self.rect.x)

    def update(self):
        self.y += 0.2
        self.rect.y = self.y