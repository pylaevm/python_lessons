import pygame
from pygame.sprite import Sprite

class Star(Sprite):
    """docstring for Star"""
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen

        image = pygame.image.load ('image/star.png').convert_alpha()
        self.image = pygame.transform.scale(image, (80, 50))
        self.rect = self.image.get_rect()

        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        self.y = float (self.rect.y)
        self.x = float (self.rect.x)

    def update(self):
        self.rect.y = self.y