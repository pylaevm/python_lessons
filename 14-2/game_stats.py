import pygame

class GameStats(object):
    """docstring for GameStats"""
    def __init__(self, ai_game):
        self.settings = ai_game.settings
        self.ships_left = self.settings.ship_limit
        self.game_active = False
        self.aliens_killed = 0

    def reset_stats(self):
        self.ships_left = self.settings.ship_limit