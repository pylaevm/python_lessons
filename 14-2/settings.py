class Settings():
    """Инициализирует настройки игры"""
    def __init__(self):
    # Параметры экрана
        self.width = 1366
        self.height = 720
        self.bg_color = (120, 70, 150)
        self.ship_limit = 3

        self.bullet_width = 10
        self.bullet_height = 3
        self.bullet_color = (60, 60, 60)
        self.bullets_allowed = 5

        self.target_x_position = 0
        # -1 up, 1 - down
        self.target_direction = 1

        self.speedup_scale = 1.1
        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        self.ship_speed = 1.5
        self.target_speed = 1.3
        self.target_drop_speed = 50
        self.bullet_speed = 1
        
        self.ship_speed_factor = 1.2
        self.bullet_speed_factor = 1.3
        self.target_speed_factor = 1.1

        #1 - вправо, -1 - влево
        self.target_direction = 1

    def increase_speed(self):
        self.ship_speed_factor *= self.speedup_scale
        self.bullet_speed_factor *= self.speedup_scale
        self.target_speed_factor *= self.speedup_scale

        self.target_speed *= self.target_speed_factor
        self.ship_speed *= self.ship_speed_factor
        self.bullet_speed *= self.bullet_speed_factor
