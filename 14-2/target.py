import pygame
from pygame.sprite import Sprite

class Target(Sprite):
    """docstring for Target"""
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.screen_rect = self.screen.get_rect()

        image = pygame.image.load ('images/sniper.png')
        self.image = pygame.transform.scale(image, (80, 50))
        self.rect = self.image.get_rect()
        print (f"Target rect {self.rect}")

        self.x = self.settings.target_x_position
        self.rect.x = self.x
        self.rect.y = self.rect.height

        self.y = float (self.rect.y)

    def check_edges(self):
        #Return true if star in screen border
        if self.rect.bottom >= self.screen_rect.bottom or self.rect.top <= 0:
            return True
        return False

    def update(self):
        self.y += (self.settings.target_speed * self.settings.target_direction)
        self.rect.y = self.y
        self.x += (self.settings.target_speed * -0.15)
        self.rect.x = self.x