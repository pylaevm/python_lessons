import sys

import pygame

from settings import Settings
from ship import Ship
from bullet import Bullet
from target import Target
from time import sleep
from game_stats import GameStats
from button import Button

class TargetGame(object):
    """класс для управления ресурсами и поведением игры TargetGame"""
    def __init__(self):
        pygame.init ()
        self.settings = Settings ()
        self.screen = pygame.display.set_mode ((0, 0), pygame.FULLSCREEN)
        self.screen_rect = self.screen.get_rect()
        pygame.display.set_caption ("Target Game")

        #Статистика игры
        self.stats = GameStats(self)

        # Фон
        self.bg_color = self.settings.bg_color
        self.ship = Ship (self)
        self.bullets = pygame.sprite.Group()
        self.target = pygame.sprite.Group()

        self.mixer = pygame.mixer.init()
        self.bg_track = pygame.mixer.Sound('sounds/imperial_march.wav')
        self.bg_track.play(-1, 0, 500)

        self._init_atarget_pos()
        self._create_target()

        self.play_button = Button(self, "Попади, сука")

    def run_game(self):
        """Запуск основного цикла игры"""
        while True:
            self._check_events()
            if self.stats.game_active:
                self.ship.update()
                self._update_bullets()
                self._update_targets()

            self._update_screen()

    def _update_bullets(self):
        #Обновление позиций снарядов
        self.bullets.update()

        #Удаление старых снарядов
        for bullet in self.bullets.copy():
            if bullet.rect.right >= self.screen.get_rect().right:
                self.bullets.remove(bullet)

        self._check_bullet_target_collision()

    def _check_bullet_target_collision(self):
        collisions = pygame.sprite.groupcollide(
                     self.bullets, self.target, True, True)

        if not self.target:
            self.bullets.empty()
            self._create_target()
            self.settings.increase_speed()

    def _init_atarget_pos(self):
        target = Target(self)
        self.settings.target_x_position = self.screen_rect.width - target.rect.width

    def _update_targets(self):
        self._check_target_edges()
        #Обновление всех позиций Звезд
        self.target.update()

        if pygame.sprite.spritecollideany(self.ship, self.target):
            self._ship_hit()

        self._check_target_left()

    #Обработка столкновений корабля с пришельцами
    def _ship_hit(self):
        if self.stats.ships_left > 0:
            self.stats.ships_left -= 1

            #Очистка списка пришельцев и снарядов
            self.target.empty()
            self.bullets.empty()

            #Создание нового корабля и цели
            self._create_target()
            self.ship.center_ship()

            sleep(0.5)
        else:
            self.stats.game_active = False
            self.settings.initialize_dynamic_settings()


    def _check_events(self):
        # Отслеживание событий клавиатуры и мыши
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                self._check_play_button(mouse_pos)

    def _check_play_button(self, mouse_pos):
        button_clicked = self.play_button.rect.collidepoint(mouse_pos)
        if button_clicked and not self.stats.game_active:
            self._start_game()

    def _start_game(self):
        if self.stats.game_active:
            return None
        self.stats.reset_stats()
        self.stats.game_active = True

        self.target.empty()
        self.bullets.empty()

        self.settings.target_x_position = self.screen_rect.width - 100
        self.settings.initialize_dynamic_settings()
        
        self._create_target()
        self.ship.center_ship()
        pygame.mouse.set_visible(False)

    def _check_keydown_events (self,event):
        if event.key == pygame.K_UP:
            self.ship.moving_up = True
        elif event.key == pygame.K_DOWN:
            self.ship.moving_down = True
        elif event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()
        elif event.key == pygame.K_p:
            self._start_game()


    def _check_keyup_events (self,event):
        if event.key == pygame.K_UP:
            self.ship.moving_up = False
        elif event.key == pygame.K_DOWN:
            self.ship.moving_down = False

    def _check_target_left(self):
        screen_rect = self.screen.get_rect()
        for t in self.target.sprites():
            if t.rect.left <= screen_rect.left:
                self._ship_hit()
                break

    def _fire_bullet(self):
        #Создание нового снаряда
        if len(self.bullets) < self.settings.bullets_allowed:
            new_bullet = Bullet(self)
            self.bullets.add(new_bullet)

    #Создание Цели
    def _create_target(self):
        target = Target(self)
        self.target.add(target)

    #Проверка достижения края экрана флотом и смена направления флота
    def _check_target_edges(self):
        for t in self.target.sprites():
            if t.check_edges():
                self._change_target_direction()
                break

    def _change_target_direction(self):
        self.settings.target_x_position -= self.settings.target_drop_speed
        self.settings.target_direction *= -1


    def _update_screen(self):
        self.screen.fill(self.settings.bg_color)
        self.ship.blitme()

        for bullet in self.bullets.sprites():
            bullet.draw_bullet()

        self.target.draw(self.screen)

        #Отображение кнопки
        if not self.stats.game_active:
            pygame.mouse.set_visible(True)
            self.play_button.draw_button()


        # Отображение последнего прорисованного экрана
        pygame.display.flip()



if __name__ == '__main__':
    ai = TargetGame()
    ai.run_game()