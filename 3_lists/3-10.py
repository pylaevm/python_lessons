any = ["Plane", "table", "status", "visibility", "energy", "freedom", "salmon", "mushrooms", "mustache"]

print (any)
print(any[2])
print(any[-2])

any.append("silicon")
print (any)

del any[-1]
print (any)

any.insert(3, "level")
print (any)

last = any.pop()
print (f"poped {last}")
print (any)

vis = "visibility"
any.remove(vis)
print (f"removed '{vis}'")
print (any)

print ('sorted')
print (sorted(any))
print (any)
print ('sorted with reverse flag')
print (sorted(any, reverse=True))
print (any[7])
any.sort()
print ('sort function')
print (any)
print (any[7])