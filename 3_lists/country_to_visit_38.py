countries = []
country = ""

while country != "x":
	country = input ("Какую страну вы бы хотели посетить? (print 'x' to stop)")
	if country == "x":	
		break
	countries.append (country)

print (countries)
print ('sorted')
print (sorted(countries))
print (countries)
print ('sorted with reverse flag')
print (sorted(countries, reverse=True))
print (countries)
print ('reversed')
countries.reverse()
print (countries)
print ('unreversed')
countries.reverse()
print (countries)
print ('sort function')
countries.sort()
print (countries)