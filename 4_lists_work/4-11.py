pizza = ["Four_Cheese", "Havai", "Celebrity", "Meat"]
friend_pizza = pizza[:]

pizza.append("DogPizza")
friend_pizza.append("HotDogPizza")
print(pizza)
print(friend_pizza)

print("My favourite pizzas:")
for p in pizza:
    print(f"I like '{p.lower()}' pizza")

print("Friends favourite pizzas:")
for p in friend_pizza:
    print(f"I like '{p.lower()}' pizza")
