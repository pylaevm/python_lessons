# Classic make list & append vals to it
print("classic")
squares = []
for i in range(1, 11):
    squares.append(i**2)
print(squares)

print("modern")
# Working make list &append vals to it
squares2 = [s**2 for s in range(1,11)]
print(squares2)
print("")
# Make a list from range with current step in one string
# first i - out val
l = [i for i in range(1,100,4)]
print(l)
print(f"Max from list {max(l)}")
print(f"Min from list {min(l)}")
print(f"Summ from list {sum(l)}")