current_users = ['Ivan', 'Slava', 'Vitja', 'Maxim', 'Igor', 'admin']
# Формирование копии списка с заменой регистра
low_current = [u.lower() for u in current_users]

new_users = ['Vano', 'slava', 'Jaroslav', 'Maxim', 'Sam', 'Andrew']

for nu in new_users:
    if nu.lower() in low_current:
        print (f"The {nu} name is bisy.Choose another name")
    else:
        print (f"The {nu} name is available.")
