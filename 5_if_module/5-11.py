nums = [n for n in range(1,10)]

end = ''
for n in nums:
    if n == 1:
        end = 'st'
    elif n == 2:
        end = 'nd'
    elif n == 3:
        end = 'rd'
    else:
        end = 'th'
    print (f"{n}{end}")