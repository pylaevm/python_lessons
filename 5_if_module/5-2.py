car = 'subaru'
car2 = 'audi'
cars = ('audi', 'citroen', 'peugeot')

if car not in cars:
    print (f"{car} not in the list {cars} list")
else:
    print (f"{car} in the {cars} list")

if car2 in cars:
    print (f"{car2} in the list {cars} list")
else:
    print (f"{car2} not in the {cars} list")

number = 5
print (f"number is {number}")
print (f"number < 12 {number < 12}")
print (f"number > 12 {number > 12}")
print (f"number < 5 {number < 5}")
print (f"number > 5 {number > 5}")
print (f"number >= 5 {number >= 5}")
print (f"number <= 5 {number <= 5}")
print (f"number != 5 {number != 5}")


print (f"number < 6 and number > 0 {number < 6 and number > 0}")
print (f"number > 7 or number > 0 {number > 7 or number > 0}")
