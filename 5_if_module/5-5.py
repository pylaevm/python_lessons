alien_color = input ("print color 'green', 'yellow' or 'red': ")
score = 0

if alien_color == 'green':
    score = 5
elif alien_color == 'yellow':
    score = 10
elif alien_color == 'red':
    score = 15

print (f"You just earned {score} scores!")
