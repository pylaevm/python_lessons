names = ['Ivan', 'Slava', 'Vitja', 'Maxim', 'Igor', 'admin']

for n in names:
    if n == 'admin':
        print ("Hello admin, would you like a cup of tea?")
    else:
        print (f"Hello, {n.title()}, glad to see you again!")