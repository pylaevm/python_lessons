cities = {'Denver': {'country': 'Canada', 'population' : '300000', 'fact': 'One interesting'},
          'Toronto': {'country': 'Canada', 'population' : '1200000', 'fact': 'Toronto maiden '},
          'Moscow': {'country': 'Russia', 'population' : '12000000', 'fact': 'Worst traffic'}, 
          'Chelyabinsk': {'country': 'Russia', 'population' : '5000000', 'fact': 'Auwfull ecology'} 
          }

for city, data in cities.items():
    country = data['country']
    population = data['population']
    fact = data['fact']

    print (f"The city '{city.title()}':\n\tis placed in {country}"
           f" with population amounth {population} ppl. and "
           f" interesting fact: {fact}")