glossure = {'list': 'This is immutable sequence of different kind of data',
            'tuple': 'This is mutable sequence of data',
            'dict': 'This is immutable sequence of pairs key:values',
            'Variable': 'Named memory place, interpreted current choise',
            'Loop': 'Part of the code repeated by some decisions',
            'Loop': 'Part of the code repeated by some decisions',
            'Loop': 'Part of the code repeated by some decisions'}

for k,v in glossure.items():
    print (f"\n The word: '{k.title()}'")
    print (f"Description:\t '{v}'")