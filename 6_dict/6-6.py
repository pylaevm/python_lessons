fav_lang = {'Rita': 'Ruby', 'Victor': 'C', 'Maxim': 'Python', 'Slava': 'Objective-C'}

to_vote = ('Victor', 'Loisa', 'Misha', 'Slava', 'Igor')

# Перебор всех людей для проверки наличия проголосовавших
for name in to_vote:
    if name in fav_lang.keys():
        print (f"Hello, {name.title()}. Your favourite lang is {fav_lang[name]}. Thanks for poll")
    else:
        print (f"Hello, {name.title()}. We are wait you poll.")