people = {
'Angella': {'firs_name': 'Angella', 'last_name' : 'Smith', 'age': '26', 'city': 'Los-Angeles'},
'Victor': {'firs_name': 'Victor', 'last_name' : 'Vooten', 'age': '20', 'city': 'Malibu'},
'Juri':{'firs_name': 'Juri', 'last_name' : 'Gagarin', 'age': '32', 'city': 'Moscow'}
}
for name, dictt in people.items():
    print (f"\nName(dict key): {name.title()}")
    print (f"\tFirst_name: {dictt['firs_name']}")
    print (f"\tLast_name: {dictt['last_name'].title()}")
    print (f"\tAge: {dictt['age'].title()}")
    print (f"\tCity: {dictt['city'].title()}")
