Red = {'Pet type': 'dog', 'owner': 'Matthew'}
Dolly = {'Pet type': 'sheep', 'owner': 'Abdula'}
Molly = {'Pet type': 'cat', 'owner': 'Sarah'}

pets = [Red, Dolly, Molly]

for pet in pets:

    print (f"Pet type '{pet['Pet type'].upper()}' and owner '{pet['owner']}'")
