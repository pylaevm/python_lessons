#По именам вывести любимые места человека, когда ключ это любимое место
favorite_places = {'Seacost': ['Andy', 'Maria', 'Stevie', 'Maison'], 
                   'Mountains': ['Max', 'John', 'Ben', 'Maria'],
                   'Forest': ['Valery', 'Maison', 'Patrck']}
all_names = []
for place, name in favorite_places.items():
    #print (f"\n{place} is favorite place for:")
    all_names.extend(name)
    #for n in name:
    #    print(f"\t{n.title()}")

# Собираю всех людей, удаляю повторяющихся
print(all_names)
all_names = sorted(set(all_names))
print(all_names)

# Формирую списки по любимым местам
sea_list = favorite_places['Seacost']
rocks_list = favorite_places['Mountains']
forest_list = favorite_places['Forest']

# Отмечаю вхождение из общего списка людей к конкретному
for name in all_names:
    if name in sea_list:
        print (f"The {name} favorite place is Seacost")
    if name in rocks_list:
        print (f"The {name} favorite place is Rocks")
    if name in forest_list:
        print (f"The {name} favorite place is Forest")

