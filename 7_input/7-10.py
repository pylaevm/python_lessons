
active = True
places = []
while active:

    str = input ("Где бы вы хотели провести отпуск своей мечты? (quit - чтобы завершить опрос)")

    if str == 'quit':
        active = False
    else:
        places.append (str.title())

i = 0
while i < len(places):
    print (f"Ваш отпуск мечты пройдет на {places[i].title()}")
    i+=1;
