
str = ""
active = True

while active:
    str = input ("Введите топпинг для пиццы или 'quit' - чтобы закончить: ")

    if str == 'quit':
        active = False
        break
    else:
        print (f"Для вашей пиццы был добавлен топпинг: {str.title()}")

