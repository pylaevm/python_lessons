def make_shirt (size = 'L', title = "I love Python"):
    print (f"You shirt {size} size with print {title.title()} is ready!")

make_shirt ()
make_shirt (52, "Live is beauty")
make_shirt (title = "Code, eat, sleep, repeat", size = 50)