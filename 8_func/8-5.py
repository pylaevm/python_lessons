def describe_city (city_name, country_name = "Russia"):
    print (f"{city_name} is in {country_name.title()}!")

describe_city ("Voskresensk")
describe_city ("Saratov")
describe_city ("Moscow")
describe_city ("Washington", "USA")
