def describe_city (city_name, country_name = "Russia"):
    sent = f"{city_name}, {country_name.title()}!"
    return sent

print (describe_city ("Voskresensk"));
print (describe_city ("Saratov"));
print (describe_city ("Moscow"));
print (describe_city ("Washington", "USA"));
