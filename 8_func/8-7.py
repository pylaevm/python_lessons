def make_album (author, album, traks = None):
    disc = {'author' : author, 'album' : album}
    if traks:
        disc['traks'] = traks
    return disc

print (make_album ("Vooten", 'Tss'))
print (make_album ("Raibow", 'Difficult to cure'))
print (make_album ("Iron Maiden", 'Fear of the dark', 12))
