def make_album (author, album, traks = None):
    disc = {'author' : author, 'album' : album}
    if traks:
        disc['traks'] = traks
    return disc

active = True
while active:
    author = input ("Enter the author name, or 'quit' to exit: ")
    if author == 'quit':
        active = False
        continue;

    album = input ("Enter the album name: ")

    print (f"You entered {make_album (author, album)}")


