from random import randint

class Die():
    """docstring for Die"side"""
    def __init__(self,side=6):
        self.side = side

    def roll_side(self, num):
        print (f"{num}. You got {randint(1, self.side)}")


die = Die()
for i in range(1,11):
    die.roll_side(i)

die = Die(10)
for i in range(1,11):
    die.roll_side(i)

die = Die(20)
for i in range(1,11):
    die.roll_side(i)