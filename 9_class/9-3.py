class User():
    def __init__ (self, first_name, last_name):
        self.first_name = first_name
        self.last_name  = last_name
        self.age = 33
        self.email = 'example@ex.exa'

    def describe_user (self):
        print (f"User name {self.first_name},{self.last_name} age {self.age}"
               f" email {self.email}")

    def greet_user (self):
        print (f"Welcome {self.first_name.title()}")


u = User ('Maxim', 'Pylaev')
u.greet_user()
u.describe_user()
u1 = User ('Tanja', 'Pylaeva')
u1.greet_user()
u1.describe_user()
u2 = User ('Egor', 'Pylaev')
u2.greet_user()
u2.describe_user()
u3 = User ('Luba', 'Pylaeva')
u3.greet_user()
u3.describe_user()
