class Restaurant():
    """docstring for Restaurant"""
    def __init__(self, name, cuisine):
        self.name = name
        self.cuisine = cuisine
        self.number_served = 0

    def describe_restaurant (self):
        print (f"Restaurant name: {self.name.title()} and cuisine {self.cuisine}")

    def open_resraurant (self):
        print ("The restaurant is open!")

    def set_number_served (self, number):
        self.number_served = number

    def inc_served (self, count):
        self.number_served += count

rest = Restaurant ('Molly', 12)
rest.describe_restaurant ()
rest.open_resraurant ()

print (f"Number served visitors is {rest.number_served}")
rest.number_served = 12
print (f"Number served visitors is {rest.number_served}")
rest.set_number_served (10)
print (f"Number served visitors is {rest.number_served}")
rest.inc_served(45)
print (f"Number served visitors is {rest.number_served}")