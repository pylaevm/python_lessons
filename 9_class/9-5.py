class User():
    def __init__ (self, first_name, last_name):
        self.first_name = first_name
        self.last_name  = last_name
        self.age = 33
        self.email = 'example@ex.exa'
        self.login_attempts = 0

    def describe_user (self):
        print (f"User name {self.first_name},{self.last_name} age {self.age}"
               f" email {self.email}")

    def greet_user (self):
        print (f"Welcome {self.first_name.title()}")

    def increment_login_attempts (self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0


user = User ('Maxim', 'Pylaev')
user.increment_login_attempts()
user.increment_login_attempts()
user.increment_login_attempts()
print (f"User attempts: {user.login_attempts}")
user.reset_login_attempts()
print (f"User attempts after reset: {user.login_attempts}")