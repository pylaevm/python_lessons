class Restaurant():
    """docstring for Restaurant"""
    def __init__(self, name, cuisine):
        self.name = name
        self.cuisine = cuisine
        self.number_served = 0

    def describe_restaurant (self):
        print (f"Restaurant name: {self.name.title()} and cuisine {self.cuisine}")

    def open_resraurant (self):
        print ("The restaurant is open!")

    def set_number_served (self, number):
        self.number_served = number

    def inc_served (self, count):
        self.number_served += count


class IceCreamStand (Restaurant):
    def __init__ (self, name, cuisine):
        super().__init__ (name, cuisine)
        self.flavors = ['chocolate', 'eskimo', 'bottle']

    def print_flavors (self):
        for fl in self.flavors:
            print (f"the flavor of icecream '{fl}'")


ice = IceCreamStand ("IceMice", '12')
ice.print_flavors()