class User():
    def __init__ (self, first_name, last_name):
        self.first_name = first_name
        self.last_name  = last_name
        self.age = 33
        self.email = 'example@ex.exa'

    def describe_user (self):
        print (f"User name {self.first_name},{self.last_name} age {self.age}"
               f" email {self.email}")

    def greet_user (self):
        print (f"Welcome {self.first_name.title()}")


class Privilegies():
    def __init__ (self):
        self.privilegies = ['adding message', 'allow user block', 'allow user delete']

    def show_privilegies (self):
        print (self.privilegies)

class Admin (User):
    def __init__(self, first_name, last_name):
        super().__init__(first_name, last_name)
        self.privilegies = Privilegies ()



adm = Admin("Maxim", "Pylaev")
adm.privilegies.show_privilegies()