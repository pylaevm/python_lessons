class Car ():
    """docstring for Car"""
    def __init__ (self, make, model, year):
        self.make  = make
        self.model = model
        self.year  = year
        self.odometer_reading = 0

    def get_descriptive_name (self):
        long_name = f"{self.make} {self.model} {self.year}"
        return long_name.title()

    def read_odometer (self):
        print (f"This car has {self.odometer_reading} miles on it")

    def update_odometer (self, mileage):
        if mileage >= self.odometer_reading:
            self.odometer_reading = mileage
        else:
            print ("You can't roll back odometer")

    def increment_odometer (self, miles):
        self.odometer_reading += miles


class Baterry ():
    def __init__ (self, baterry_size=75):
        self.baterry_size = 75

    def get_range (self):
        if self.baterry_size == 75:
            range = 260
        elif self.baterry_size == 100:
            range = 315

        print (f"This car can go about {range} miles on a full charge")

    def upgrade_baterry (self):
        if self.baterry_size != 100:
            self.baterry_size = 100


class ElectricCar (Car):
    def __init__ (self, make, model, year):
        super().__init__(make, model, year)
        self.battery = Baterry ()

    def describe_baterry (self):
        print (f"This car has a {self.baterry.battery_size}-kWh baterry")

    def fill_gas_tank (self):
        print ("You can't charge electric car with gasoline")


ec = ElectricCar('subary', 'outback', 2019)
ec.battery.get_range ()
ec.battery.upgrade_baterry()
ec.battery.get_range ()

        