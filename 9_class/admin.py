from user import User

class Privilegies():
    def __init__ (self):
        self.privilegies = ['adding message', 'allow user block', 'allow user delete']

    def show_privilegies (self):
        print (self.privilegies)

class Admin (User):
    def __init__(self, first_name, last_name):
        super().__init__(first_name, last_name)
        self.privilegies = Privilegies ()
