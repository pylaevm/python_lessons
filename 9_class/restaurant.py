class Restaurant():
    """docstring for Restaurant"""
    def __init__(self, name, cuisine):
        self.name = name
        self.cuisine = cuisine

    def describe_restaurant (self):
        print (f"Restaurant name: {self.name.title()} and cuisine {self.cuisine}")

    def open_resraurant (self):
        print ("The restaurant is open!")
