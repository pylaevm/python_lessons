import sys

import pygame

from settings import Settings
from ship import Ship
from bullet import Bullet
from alien import Alien
from time import sleep
from game_stats import GameStats
from button import Button
from scoreboard import Scoreboard

class AlienInvasion(object):
    """класс для управления ресурсами и поведением игры AlienInvasion"""
    def __init__(self):
        pygame.init ()
        pygame.display.init()
        self.settings = Settings ()
        self.screen = pygame.display.set_mode ((self.settings.width, self.settings.height), pygame.FULLSCREEN)
        pygame.display.set_caption ("Alien Invasion")

        #Статистика игры
        self.stats = GameStats(self)
        self.scoreboard = Scoreboard(self)

        # Фон
        self.bg_color = self.settings.bg_color
        #Коабль
        self.ship = Ship (self)
        #Пули
        self.bullets = pygame.sprite.Group()
        #Пришельцы
        self.aliens = pygame.sprite.Group()

        #Звук
        self.mixer = pygame.mixer.init()
        self.bg_track = pygame.mixer.Sound('sounds/imperial_march.wav')

        #Создание флота пришельцев
        self._create_fleet()

        self.play_buttons = pygame.sprite.Group()
        self._create_buttons()
        

    def run_game(self):
        """Запуск основного цикла игры"""
        self.bg_track.play(-1, 0, 500)
        while True:
            self._check_events()
            if self.stats.game_active:
                self.ship.update()
                self._update_bullets()
                self._update_aliens()

            self._update_screen()

    def _update_bullets(self):
        #Обновление позиций снарядов
        self.bullets.update()

        #Удаление старых снарядов
        for bullet in self.bullets.copy():
            if bullet.rect.bottom <= 0:
                self.bullets.remove(bullet)

        self._check_bullet_alien_collision()

    def _check_bullet_alien_collision(self):
        collisions = pygame.sprite.groupcollide(self.bullets,
                                                self.aliens,
                                                True, True)

        #Ведение счета за сбитые корабли пришельцев
        if collisions:
            self._count_score(collisions)

        if not self.aliens:
            self._start_new_level()

    def _count_score(self, collisions):
        for aliens in collisions.values():
            self.stats.score += self.settings.alien_points * len(aliens)
        self.scoreboard.prep_score()
        #Корректировка рекорда
        self.scoreboard.check_high_score()

    def _start_new_level(self):
        self.bullets.empty()
        self._create_fleet()
        self.settings.increase_speed()

        self.stats.level += 1
        self.scoreboard.prep_level()


    def _update_aliens(self):
        self._check_fleet_edges()
        #Обновление всех позиций пришельцев
        self.aliens.update()

        if pygame.sprite.spritecollideany(self.ship, self.aliens):
            self._ship_hit()

        self._check_aliens_bottom()

    #Обработка столкновений корабля с пришельцами
    def _ship_hit(self):
        if self.stats.ships_left > 0:
            self.stats.ships_left -= 1
            self.scoreboard.prep_ships()

            #Очистка списка пришельцев и снарядов
            self.aliens.empty()
            self.bullets.empty()

            #Создание нового корабля и флота
            self._create_fleet()
            self.ship.center_ship()

            sleep(0.5)
        else:
            self.stats.game_active = False

    def _create_buttons(self):
        button_easy = Button (self, "Easy", 1)
        button_reg = Button (self, "Regular", 2)
        button_hard = Button (self, "Hard", 3)
        self.play_buttons.add (button_easy)
        self.play_buttons.add (button_reg)
        self.play_buttons.add (button_hard)

    # Отслеживание событий клавиатуры и мыши
    def _check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                self._check_play_button(mouse_pos)

    def _check_play_button(self, mouse_pos):
        for button in self.play_buttons.sprites():
            button_clicked = button.rect.collidepoint(mouse_pos)
            if button_clicked and not self.stats.game_active:

                self.settings.initialize_dynamic_settings(button.get_level())
                self._start_game()
                break

    def _start_game(self):
        if self.stats.game_active:
            return None
        self.stats.reset_stats()
        self.stats.game_active = True
        self.scoreboard.prep_score()
        self.scoreboard.prep_level()
        self.scoreboard.prep_ships()
        self.aliens.empty()
        self.bullets.empty()

        self._create_fleet()
        self.ship.center_ship()
        pygame.mouse.set_visible(False)


    def _check_keydown_events (self,event):
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = True
        elif event.key == pygame.K_q:
            self.settings.set_hscore(self.stats.high_score)
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()
        elif event.key == pygame.K_p:
            self._start_game()


    def _check_keyup_events (self,event):
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = False

    def _check_aliens_bottom(self):
        screen_rect = self.screen.get_rect()
        for alien in self.aliens.sprites():
            if alien.rect.bottom >= screen_rect.bottom:
                self._ship_hit()
                break

    #Создание нового снаряда
    def _fire_bullet(self):
        if len(self.bullets) < self.settings.bullets_allowed:
            new_bullet = Bullet(self)
            self.bullets.add(new_bullet)

    #Создание флота пришельцев
    def _create_fleet(self):
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size
        alien_number_x = self._get_alien_number(alien_width)
        row_numbers = self._get_row_number(alien_height)

        for row_number in range(row_numbers):
            for alien_number in range(alien_number_x):
                self._create_alien(alien_number, row_number)

    # Количество пришельцев в ряду
    def _get_alien_number(self, alien_width):
        available_space_x = self.settings.width - (2 * alien_width)
        number_aliens_x = available_space_x // (2 * alien_width)
        return number_aliens_x

    #Количество рядов пришельцев
    def _get_row_number(self, alien_height):
        #Определяю количество рядов на экране
        ship_height = self.ship.rect.height
        available_space_y = (self.settings.height - 
                            (3 * alien_height - ship_height))

        number_rows = available_space_y // (2 * alien_height)
        return number_rows

    #Создание одного пришельца
    def _create_alien(self, alien_number, row_number): 
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size
        alien.x = alien_width + 2 * alien_width * alien_number
        alien.rect.x = alien.x
        alien.rect.y = alien_height + 2 * alien_height * row_number
        self.aliens.add(alien)

    #Проверка достижения края экрана флотом и смена направления флота
    def _check_fleet_edges(self):
        for alien in self.aliens.sprites():
            if alien.check_edges():
                self._change_fleet_direction()
                break

    def _change_fleet_direction(self):
        for alien in self.aliens.sprites():
            alien.rect.y += self.settings.fleet_drop_speed
        self.settings.fleet_direction *= -1

    def _update_screen(self):
        self.screen.fill(self.settings.bg_color)
        self.ship.blitme()

        for bullet in self.bullets.sprites():
            bullet.draw_bullet()

        self.aliens.draw(self.screen)

        self.scoreboard.show_score()

        #Отображение кнопки
        if not self.stats.game_active:
            pygame.mouse.set_visible(True)
            for b in self.play_buttons.sprites():
                b.draw_button()

        # Отображение последнего прорисованного экрана
        pygame.display.flip()



if __name__ == '__main__':
    ai = AlienInvasion()
    ai.run_game()