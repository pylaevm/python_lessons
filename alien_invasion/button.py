import pygame.font
from pygame.sprite import Sprite

class Button(Sprite):
    """docstring for Button"""
    def __init__(self, ai_game, msg, level=1):
        super().__init__()
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()
        self.level = level
        
        level_color = self._prep_color(level)

        self.width, self.height = 200, 50
        self.button_color = level_color
        self.text_color = (255, 255, 255)
        self.font = pygame.font.SysFont(None, 48)

        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self._place_button(level)

        self._prep_msg(msg)

    def _prep_color(self, level):
        if level == 1:
            level_color = (0, 255, 0)
        elif level == 2:
            level_color = (255, 255, 0)
        elif level == 3:
            level_color = (255, 0, 0)
        
        return level_color

    def _place_button(self, level):
        if level == 1:
            self.rect.midleft = self.screen_rect.midleft
        elif level == 2:
            self.rect.center = self.screen_rect.center
        elif level == 3:
            self.rect.midright = self.screen_rect.midright

    def _prep_msg(self, msg):
        self.msg_image = self.font.render(msg, True, self.text_color, self.button_color)

        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center

    def get_level (self):
        return self.level

    def draw_button(self):
        self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)