import pygame

class GameStats(object):
    """docstring for GameStats"""
    def __init__(self, ai_game):
        self.settings = ai_game.settings
        self.ships_left = self.settings.ship_limit
        self.game_active = False
        self.score = 0
        self.high_score = self.settings.hscore
        self.level = 0

    def reset_stats(self):
        self.ships_left = self.settings.ship_limit
        self.score = 0
        self.level = 1