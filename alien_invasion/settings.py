class Settings():
    """Инициализирует настройки игры"""
    def __init__(self):
        # Параметры экрана
        self.width = 1366
        self.height = 720
        self.bg_color = (30, 70, 150)
        self.ship_limit = 3

        #Параметры пули
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = (60, 60, 60)
        self.bullets_allowed = 5

        #Скороссть снижения флота
        self.fleet_drop_speed = 10
        # -1 left, 1 - right
        self.fleet_direction = 1

        #Увеличение скорости по уровням
        self.speedup_scale = 1.1

        #Множитель очков по уровням
        self.score_scale = 1.5

        self.hscore_file = "score.txt"
        self.hscore = self.get_hscore()

        self.initialize_dynamic_settings()

    def get_hscore(self):
        try:
            with open(self.hscore_file, "r") as f:
                score = f.read()
            score = int (score)

        except FileNotFoundError:
            score = 0
        return score

    def set_hscore(self, hscore):
        with open(self.hscore_file, "w") as f:
            f.write(str(hscore))

    def _reset_speed(self):
        self.ship_speed = 1.5
        self.bullet_speed = 2
        self.alien_speed = 1.2

    #Инициализация начальных настроек в зависимости от уровня level
    def initialize_dynamic_settings(self, level=1):
        self.alien_points = 50
        
        self._reset_speed()
        if level == 1:
            self.ship_speed_factor = 1.3
            self.bullet_speed_factor = 1.3
            self.alien_speed_factor = 1.1
        elif level == 2:
            self.ship_speed_factor = 1.2
            self.bullet_speed_factor = 1.2
            self.alien_speed_factor = 1.2
        elif level == 3:
            self.ship_speed_factor = 1
            self.bullet_speed_factor = 1.1
            self.alien_speed_factor = 1.3

        #1 - вправо, -1 - влево
        self.fleet_direction = 1

    def increase_speed(self):
        self.ship_speed_factor *= self.speedup_scale
        self.bullet_speed_factor *= self.speedup_scale
        self.alien_speed_factor *= self.speedup_scale

        self.alien_speed *= self.alien_speed_factor
        self.ship_speed *= self.ship_speed_factor
        self.bullet_speed *= self.bullet_speed_factor

        self.alien_points = int(self.alien_points * self.score_scale)
        print(f"Points for alien {self.alien_points}")
