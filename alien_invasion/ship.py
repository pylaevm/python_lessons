import pygame
from pygame.sprite import Sprite

class Ship(Sprite):
    """docstring for Ship"""
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen
        self.screen_rect = ai_game.screen.get_rect()
        self.settings = ai_game.settings

        # Масштабирование изображения
        image = pygame.image.load('images/rocket.png').convert_alpha()

        self.image = pygame.transform.scale(image, (50,100))

        # Позиционирование на экране
        self.rect = self.image.get_rect()
        print (self.rect)
        # Новый корабль появляется в нижнем центре окна
        self.rect.midbottom = self.screen_rect.midbottom

        # Флаги перемещения
        self.moving_right = False
        self.moving_left  = False

        self.x = float(self.rect.x)

    def blitme(self):
        self.screen.blit(self.image, self.rect)

    def update(self):
        # Обновление позиции корабля
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.ship_speed
        if self.moving_left and self.rect.left > 0:
            self.x -= self.settings.ship_speed

        self.rect.x = self.x

    def center_ship(self):
        self.rect.midbottom = self.screen_rect.midbottom
        self.x = float(self.rect.x)