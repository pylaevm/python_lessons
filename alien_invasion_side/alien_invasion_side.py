import sys

import pygame

from settings import Settings
from ship import Ship
from bullet import Bullet
from star import Star
from time import sleep
from game_stats import GameStats


class AlienInvasion(object):
    """класс для управления ресурсами и поведением игры AlienInvasion"""
    def __init__(self):
        pygame.init ()
        self.settings = Settings ()
        self.screen = pygame.display.set_mode ((self.settings.width, self.settings.height),pygame.FULLSCREEN)
        pygame.display.set_caption ("Star Invasion")

        #Статистика игры
        self.stats = GameStats(self)

        # Фон
        self.bg_color = self.settings.bg_color
        self.ship = Ship (self)
        self.bullets = pygame.sprite.Group()
        self.stars = pygame.sprite.Group()

        self.mixer = pygame.mixer.init()
        self.bg_track = pygame.mixer.Sound('sounds/imperial_march.wav')
        self.bg_track.play(-1, 0, 500)

        self.aliens_need_to_kill = 0
        self._create_fleet()

    def run_game(self):
        """Запуск основного цикла игры"""
        while True:
            self._check_events()
            if self.stats.game_active:
                self.ship.update()
                self._update_bullets()
                self._update_stars()

            self._update_screen()

    def _update_bullets(self):
        #Обновление позиций снарядов
        self.bullets.update()

        #Удаление старых снарядов
        for bullet in self.bullets.copy():
            if bullet.rect.right >= self.screen.get_rect().right:
                self.bullets.remove(bullet)

        self._check_bullet_star_collision()

    def _check_bullet_star_collision(self):
        collisions = pygame.sprite.groupcollide(
                     self.bullets, self.stars, True, True)

        self.stats.aliens_killed += len(collisions)

        if (self.stats.aliens_killed == self.aliens_need_to_kill):
            #Replace it with screen congrats
            print ("You won")

        if not self.stars:
            self.bullets.empty()
            self._create_fleet()

    def _update_stars(self):
        self._check_fleet_edges()
        #Обновление всех позиций Звезд
        self.stars.update()

        if pygame.sprite.spritecollideany(self.ship, self.stars):
            self._ship_hit()

        self._check_stars_left()

    #Обработка столкновений корабля с пришельцами
    def _ship_hit(self):
        if self.stats.ships_left > 0:
            self.stats.ships_left -= 1

            #Очистка списка пришельцев и снарядов
            self.stars.empty()
            self.bullets.empty()

            #Создание нового корабля и флота
            self._create_fleet()
            self.ship.center_ship()

            sleep(0.5)
        else:
            self.stats.game_active = False


    def _check_events(self):
        # Отслеживание событий клавиатуры и мыши
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)

    def _check_keydown_events (self,event):
        if event.key == pygame.K_UP:
            self.ship.moving_up = True
        elif event.key == pygame.K_DOWN:
            self.ship.moving_down = True
        elif event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()


    def _check_keyup_events (self,event):
        if event.key == pygame.K_UP:
            self.ship.moving_up = False
        elif event.key == pygame.K_DOWN:
            self.ship.moving_down = False

    def _check_stars_left(self):
        screen_rect = self.screen.get_rect()
        for star in self.stars.sprites():
            if star.rect.left <= screen_rect.left:
                self._ship_hit()
                break

    def _fire_bullet(self):
        #Создание нового снаряда
        if len(self.bullets) < self.settings.bullets_allowed:
            new_bullet = Bullet(self)
            self.bullets.add(new_bullet)

        #Создание флота пришельцев
    def _create_fleet(self):
        star = Star(self)
        star_width, star_height = star.rect.size
        star_number_y = self._get_star_in_column(star_height)
        column_numbers = self._get_column_number(star_width)
        self.aliens_need_to_kill = star_number_y * column_numbers

        for column_number in range(column_numbers):
            for star_number in range(star_number_y):
                self._create_star(star_number, column_number)

    # Количество звезд в столбце
    def _get_star_in_column(self, star_height):
        available_space_y = self.settings.height - (2 * star_height)
        number_stars_y = available_space_y // (2 * star_height)
        print (f"Number stars in column {number_stars_y}")
        return number_stars_y

    #Количество столбцов звезд
    def _get_column_number(self, star_width):
        #Определяю количество рядов на экране
        ship_width = self.ship.rect.width
        available_space_x = (self.settings.width - 
                            (3 * star_width - ship_width))

        number = available_space_x // (2 * star_width)
        print (f"Column number {number}")
        return number

    #Создание одного пришельца
    def _create_star(self, star_number, column_number): 
        star = Star(self)
        star_width, star_height = star.rect.size
        ship_width = self.ship.rect.width
        star.rect.x = ship_width + star_width + 2 * star_width * column_number
        star.rect.y = star_height + 2 * star_height * star_number
        star.y = star.rect.y
        self.stars.add(star)

    #Проверка достижения края экрана флотом и смена направления флота
    def _check_fleet_edges(self):
        for star in self.stars.sprites():
            if star.check_edges():
                self._change_fleet_direction()
                break

    def _change_fleet_direction(self):
        for star in self.stars.sprites():
            star.rect.x -= self.settings.fleet_drop_speed
        self.settings.fleet_direction *= -1


    def _update_screen(self):
        self.screen.fill(self.settings.bg_color)
        self.ship.blitme()

        for bullet in self.bullets.sprites():
            bullet.draw_bullet()

        self.stars.draw(self.screen)

        # Отображение последнего прорисованного экрана
        pygame.display.flip()



if __name__ == '__main__':
    ai = AlienInvasion()
    ai.run_game()