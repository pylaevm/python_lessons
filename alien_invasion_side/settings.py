class Settings():
    """Инициализирует настройки игры"""
    def __init__(self):
    # Параметры экрана
        self.width = 1366
        self.height = 720
        self.bg_color = (30, 70, 150)
        self.ship_speed = 1.5
        self.ship_limit = 3

        self.bullet_speed = 1
        self.bullet_width = 10
        self.bullet_height = 3
        self.bullet_color = (60, 60, 60)
        self.bullets_allowed = 5

        self.star_speed = 1
        self.fleet_drop_speed = 10
        # -1 left, 1 - right
        self.fleet_direction = 1
