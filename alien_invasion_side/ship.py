import pygame

class Ship():
    """docstring for Ship"""
    def __init__(self, ai_game):
        self.screen = ai_game.screen
        self.screen_rect = ai_game.screen.get_rect()
        self.settings = ai_game.settings

        # Масштабирование изображения
        image = pygame.image.load('images/rocket.png').convert_alpha()

        self.image = pygame.transform.scale(image, (50,100))
        self.image = pygame.transform.rotate(self.image, -90)

        # Позиционирование на экране
        self.rect = self.image.get_rect()
        print (self.rect)
        # Новый корабль появляется в нижнем центре окна
        self.rect.midleft = self.screen_rect.midleft

        # Флаги перемещения
        self.moving_up = False
        self.moving_down  = False

        self.y = float(self.rect.y)

    def blitme(self):
        self.screen.blit(self.image, self.rect)

    def update(self):
        # Обновление позиции корабля
        if self.moving_down and self.rect.bottom < self.screen_rect.bottom:
            self.y += self.settings.ship_speed
        if self.moving_up and self.rect.top > 0:
            self.y -= self.settings.ship_speed

        self.rect.y = self.y

    def center_ship(self):
        self.rect.midleft = self.screen_rect.midleft
        self.y = float(self.rect.y)