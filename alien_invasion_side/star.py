import pygame
from pygame.sprite import Sprite

class Star(Sprite):
    """docstring for Star"""
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings

        image = pygame.image.load ('images/star.png')
        self.image = pygame.transform.scale(image, (80, 50))
        self.rect = self.image.get_rect()
        print (f"Star rect {self.rect}")

        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        self.y = float (self.rect.y)

    def check_edges(self):
        #Return true if star in screen border
        screen_rect = self.screen.get_rect()
        if self.rect.bottom >= screen_rect.bottom or self.rect.top <= 0:
            return True
        return False

    def update(self):
        self.y += (self.settings.star_speed * self.settings.fleet_direction)
        self.rect.y = self.y